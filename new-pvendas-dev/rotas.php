<?php
// ini_set('display_errors', 0);
// ini_set('log_errors', 1);
// ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
//error_reporting(E_ALL);

define( 'DS', DIRECTORY_SEPARATOR );

class rotas{

  public function myRota(){

    switch ($_GET['pg']) {

      case 'cheatDetail':
        require_once 'controllers/cheatControler.php';
        $cheatController = new CheatController();
        $cheatId = $_GET['cheatId'];
        
        $response = $cheatController->cheatsDetail($cheatId);
        $date = $response['last_update']['date'];
        $last_update = new DateTime($response['last_update']['date']);
        $date_atual = new DateTime();
        $intervalo = $last_update->diff($date_atual);        
        require_once  'cheatsDetail.php';
        break;      

    	case 'checkout':
        require_once  'checkout.php';
        break;

      case 'faq':
        require_once  'faq.php';
        break;  

      default:
        require_once 'controllers/cheatControler.php';
        $cheatController = new CheatController();

        $response = $cheatController->listAllCheats();
        require_once 'home.php';
        break;
    }
  }
}

?>
