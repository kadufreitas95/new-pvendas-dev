<footer class="footer features-section py-5">
    <div class="text-center col-md-12" style="color:#8f959e">
      <div class="contanier">
        <div class="col-lg-12">
          <div class="col-lg-6 col-md-10 offset-md-2 mx-lg-auto">
            <div class="contato  text-left">
              <p>Dúvidas?<a href="mailto:contato@cheatsplace.com"> contato@cheatsplace.com</a></p>
            </div>
            <div class="row justify-content-center">
              <div class="col-lg-4 col-md-4">
                <button type="button" class="btn btn-link col-12 text-left" onclick="location.href='?pg=faq';">Perguntas Frequentes</button>
                <button type="button" class="btn btn-link col-12 text-left" onclick="location.href='https://www.cheatsplace.com/termos-de-uso.php';">Termos de Uso</button>
                <button type="button" class="btn btn-link col-12 text-left" onclick="location.href='https://www.cheatsplace.com/politica-de-privacidade.php';">Política de Privacidade </button>
              </div>
              <div class="col-lg-4 col-md-4">
                <button type="button" class="btn btn-link col-12 text-left disabled-link">Trabalhe Conosco</button>
                <button type="button" class="btn btn-link col-12 text-left disabled-link">Seja um Parceiro</button>
              </div>
              <div class="col-lg-4 col-md-4">
                <button type="button" class="btn btn-link col-12 text-left disabled-link">Blog</button>
                <button type="button" class="btn btn-link col-12 text-left forum" onclick="location.href='https://www.cheatsplace.com/forum/';">Fórum</button>               
              </div>
            </div>
            <div class="pt-5 text-center">
              <span class="left">Cheats Place © 2018. Todos os direitos reservados.</span>
            </div>
          </div>
        </div>
      </div>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="pvendas/jquery/jquery.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
  <script src="pvendas/bootstrap4/js/popper.min.js"></script>
  <script src="pvendas/bootstrap4/js/bootstrap.min.js"></script>