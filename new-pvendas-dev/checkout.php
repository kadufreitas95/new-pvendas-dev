<!doctype html>
<html lang="en">
  <?php 
      require_once  'header.php';
  ?>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <!-- load CSS -->
  <link rel="stylesheet" href="pvendas/css/geral.css"></noscript>
  <link rel="stylesheet" href="pvendas/css/mobile.css"></noscript>

<body class="checkout">  
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9X2283"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <section class="top-logo relative">
    <div class="container">
      <div class="logo text-center pt-1">
        <img src="pvendas/imgs/cheats-logo-mini.png" alt="">
      </div>
    </div>
  </section>
  <section id="steps">
    <ul class="nav justify-content-center nav-pills mb-3 py-5" id="pills-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link  active " id="passo1-tab" data-toggle="pill" href="#passo1" role="tab" aria-controls="passo1"
          aria-selected="true">Passo 1</a>
      </li>
      <i class="fas fa-angle-right" style="padding-top: 0.7rem;"></i>
      <li class="nav-item">
        <a class="nav-link " id="passo2-tab" data-toggle="pill" href="#passo2" role="tab" aria-controls="passo2"
          aria-selected="false">Passo 2</a>
      </li>
      <i class="fas fa-angle-right" style="padding-top: 0.7rem;"></i>
      <li class="nav-item">
        <a class="nav-link custom-disabled" id="passo3-tab" data-toggle="pill" href="#passo3" role="tab" aria-controls="passo3"
          aria-selected="false">Passo 3</a>
      </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active text-center pb-5 mb-5" id="passo1" role="tabpanel" aria-labelledby="passo1-tab">
        <h3>Escolha seu Plano</h3>
        <div class="col-xl-3 col-lg-5 col-md-7 mx-auto text-left">
          <ul class="change-style-ul">
            <li class="py-3">Teste por 7 dias. Se por algum motivo, dentro desse período, você não se sentir satisfeito, avise-nos e devolveremos 100% do seu investimento.</li>
            <li class="pt-3 pb-1">Sem compromisso, cancele quando quiser.</li>
            <!-- <li class="py-2">Sed lacinia enim sed finibus porttitor. Vivamus consequat, elit eu viverra lobortis</li> -->
          </ul>
          <div class="text-center mx-auto pt-3">
            <button id="button-1" type="button" class="btn btn-block css-button second steps"><span>VEJA NOSSOS PLANOS</span><i
                class="ml-auto fas fa-chevron-circle-right"></i></button>
          </div>
        </div>
      </div>
      <div class="tab-pane fade  pb-5" id="passo2" role="tabpanel" aria-labelledby="passo2-tab">
        <div class="col-12 col-md-10 col-lg-10 col-xl-6 mx-auto">
          <h5>Escolha o melhor plano para você</h5>
          <table id="plans-table" class="table">
            <thead>
              <tr class="header-hidden">
                <th scope="col"></th>
                <th width="16%" scope="col">
                  <div class="py-5 px-1 text-center bg-curso premium active" alt="100"><span>PREMIUM</span></div>
                </th>
                <th width="16%" scope="col">
                  <div class="py-5 px-1 text-center bg-curso master" alt="99"><span>MASTER</span></div>
                </th>
                <th width="16%" scope="col" class="list-cheats-basic">
                  <div id="popoverDescktop" class=" text-center bg-curso basic init dropdown" style="perspective: 800px;z-index: 2;height: 100%;"
                    data-toggle="popover" data-placement="top" data-content="Escolha o cheat">
                    <a class="toggle-button" style="position: absolute;
                    top: 0;
                    left: 0;
                    bottom: 0;
                    right: 0;
                    padding-top: 3rem;">
                      <span class="text-button">&nbsp;BASICO</span>
                    </a>
                    <img class="img-basic-select" src="" alt="">
                    <div class="text-center" >
                      <i class="fas fa-sort-down"></i>
                    </div>
                    <div class="my-dropdown">
                      <!-- CAROUSEL -->
                      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <a class="my-item col-3" href="#">
                              <img src="pvendas/imgs/games-insignias/GCHL.png" alt="9" value="39,90" data-toggle="tooltip" data-placement="right"
                                title="Grand Chase History Latino">
                            </a>
                            <a class="my-item col-3" href="#">
                              <img src="pvendas/imgs/games-insignias/GCM.png" alt="10" value="39,90" data-toggle="tooltip" data-placement="right"
                                title="Grand Chase History Madness">
                            </a>
                            <a class="my-item col-3" href="#">
                              <img src="pvendas/imgs/games-insignias/GCH.png" alt="8" value="39,90" data-toggle="tooltip" data-placement="right"
                                title="Grand Chase History">
                            </a>
                          </div>
                          <div class="carousel-item">
                            <a class="my-item col-3" href="#">
                              <img src="pvendas/imgs/games-insignias/CF.png" alt="13" value="29,90" data-toggle="tooltip" data-placement="right"
                                title="CrossFire">
                            </a>
                            <a class="my-item col-3" href="#">
                              <img src="pvendas/imgs/games-insignias/PB.png" alt="11" value="19,90" data-toggle="tooltip" data-placement="right"
                                title="Point Blank">
                            </a>
                            <a class="my-item col-3" href="#">
                              <img src="pvendas/imgs/games-insignias/PBUSA.png" alt="12" value="19,90" data-toggle="tooltip" data-placement="right"
                                title="Point Blank United States">
                            </a>
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody class="ajust-center premium">
              <tr class="header-hidden">
                <td>Preço mensal</td>
                <td>R$117,00</td>
                <td>R$67,00</td>
                <td class="price-basic">R$----</td>
              </tr>
              <tr>
                <td>Número de computadores</td>
                <td>3</td>
                <td>2</td>
                <td>1</td>
              </tr>
              <tr>
                <td>Acesso ao cheat do jogo escolhido</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
              </tr>
              <tr>
                <td>Acesso à todos os cheats de todos os jogos</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-times"></i></td>
              </tr>
              <tr>
                <td>Acesso ao Fórum VIP</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
              </tr>
              <tr>
                <td>Suporte Básico</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
              </tr>
              <tr>
                <td>Suporte Dedicado</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-times"></i></td>
                <td><i class="fas fa-times"></i></td>
              </tr>
              <!-- <tr>
                <td>Área exclusiva no Fórum VIP</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-times"></i></td>
                <td><i class="fas fa-times"></i></td>
              </tr> -->
              <tr>
                <td>Membro em destaque no Fórum VIP</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-times"></i></td>
                <td><i class="fas fa-times"></i></td>
              </tr>
              <tr>
                <td>Acesso ao conteúdo Beta</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-times"></i></td>
                <td><i class="fas fa-times"></i></td>
              </tr>
            </tbody>
          </table>
          <div id="plans-table-mobile" class="py-4">
            <div class="row fixme">
              <div class="col-4">
                <div class="py-5 px-1 text-center bg-curso premium active" alt="premium"><span>PREMIUM</span></div>
              </div>
              <div class="col-4">
                <div class="py-5 px-1 text-center bg-curso master" alt="master"><span>MASTER</span></div>
              </div>
              <div class="list-cheats-basic col-4">
                <div id="popoverMobile" class=" text-center bg-curso basic init dropdown" style="perspective: 800px;z-index: 2;height: 100%;"
                  data-toggle="popover" data-placement="top" data-content="Escolha o cheat">
                  <a class="toggle-button">
                    <span class="text-button">&nbsp;BASICO</span>
                    <img class="img-basic-select" src="" alt="">
                  </a>
                  <div class="text-center" >
                    <i class="fas fa-sort-down"></i>
                  </div>
                  <div class="my-dropdown">
                    <!-- CAROUSEL -->
                    <div id="carouselMobile" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <a class="my-item col-3" href="#">
                            <img src="pvendas/imgs/games-insignias/GCHL.png" alt="9" value="39,90" data-toggle="tooltip" data-placement="right"
                              title="Grand Chase History Latino">
                          </a>
                          <a class="my-item col-3" href="#">
                            <img src="pvendas/imgs/games-insignias/GCM.png" alt="10" value="39,90" data-toggle="tooltip" data-placement="right"
                              title="Grand Chase History Madness">
                          </a>
                          <a class="my-item col-3" href="#">
                            <img src="pvendas/imgs/games-insignias/GCH.png" alt="8" value="39,90" data-toggle="tooltip" data-placement="right"
                              title="Grand Chase History">
                          </a>
                        </div>
                        <div class="carousel-item">
                          <a class="my-item col-3" href="#">
                            <img src="pvendas/imgs/games-insignias/CF.png" alt="13" value="29,90" data-toggle="tooltip" data-placement="right"
                              title="CrossFire">
                          </a>
                          <a class="my-item col-3" href="#">
                            <img src="pvendas/imgs/games-insignias/PB.png" alt="11" value="19,90" data-toggle="tooltip" data-placement="right"
                              title="Point Blank">
                          </a>
                          <a class="my-item col-3" href="#">
                            <img src="pvendas/imgs/games-insignias/PBUSA.png" alt="12" value="19,90" data-toggle="tooltip" data-placement="right"
                              title="Point Blank United States">
                          </a>
                        </div>
                      </div>
                      <a class="carousel-control-prev" href="#carouselMobile" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselMobile" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ajust-center premium">
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Preço mensal</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <span>R$117,00</span>
                  </div>
                  <div class="col-4">
                    <span>R$67,00</span>
                  </div>
                  <div class="col-4">
                    <span class="name-basic"></span>
                    <span class="price-basic">R$----</span>
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Número de computadores</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    3
                  </div>
                  <div class="col-4">
                    2
                  </div>
                  <div class="col-4">
                    1
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Acesso ao cheat do jogo escolhido</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <td><i class="fas fa-check"></i></td>
                  </div>
                  <div class="col-4">
                    <td><i class="fas fa-check"></i></td>
                  </div>
                  <div class="col-4">
                    <td><i class="fas fa-check"></i></td>
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Acesso à todos os cheats de todos os jogos</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Acesso ao Fórum VIP</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Suporte Básico</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Suporte Dedicado</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                </div>
              </div>
              <!-- <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Área exclusiva no Fórum VIP</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                </div>
              </div> -->
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Membro em destaque no Fórum VIP</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                </div>
              </div>
              <div class="linha">
                <div class="col-12 text-center pt-4">
                  <p>Acesso ao conteúdo Beta</p>
                </div>
                <div class="row text-center">
                  <div class="col-4">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                  <div class="col-4">
                    <i class="fas fa-times"></i>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- end-plans-table-mobile -->
          <p style="font-size: 14px;color: #aba5a5;">Na escolha do plano básico você tera acesso apenas ao cheat que
            escolher.</p>
          <div class="text-center col-md-5 mx-auto">
            <button id="button-2" type="button" class="btn btn-block css-button second steps"><span>CONTINUAR</span><i
                class="ml-auto fas fa-chevron-circle-right"></i></button>
          </div>
        </div>
      </div>
      <div class="tab-pane fade pb-5 mb-5" id="passo3" role="tabpanel" aria-labelledby="passo3-tab">
        <div id="choosePayment" class="col-12 col-md-12 col-lg-10 col-xl-6 mx-auto">
          <div class="row pb-5 justify-content-center">
            <div class="col-lg-8 col-md-7">
              <h3>Último passo!</h3>
              <p><h6>Escolha um dos dois meios de pagamento abaixo</h6></p>
              <div class="mx-auto pt-3">
                <ul class="list-group">
                  <li class="list-group-item text-left">
                    <button id="btnMonetizze" type="button" class="btn btn-block btn-success payment text-center"><span>Pagar com </span><img src="pvendas/imgs/monetizze-logo.png" alt="" width="180" style="filter: brightness(1.3)"></button>
                    <div class="cards-payment pt-3">
                      <span class="payments-methods visa"></span><span class="payments-methods mastercard"></span><span class="payments-methods dinersclub"></span><span class="payments-methods elo"></span><span class="payments-methods amex"></span><span class="payments-methods boleto"></span>
                    </div>
                  </li>                       
                  <li class="list-group-item text-left" style="display:none">
                    <button id="btnPaypal" type="button" class="btn btn-block btn-outline-secondary payment text-center"><span>Pagar com </span><img src="pvendas/imgs/Paypal-logo.png" alt="" width="90"></button>
                    <div class="cards-payment pt-3">
                      <span class="payments-methods mastercard"></span><span class="payments-methods elo"></span><span class="payments-methods amex"></span><span class="payments-methods visa"></span><span class="payments-methods hiper"></span>
                    </div>
                  </li>           
                </ul>
                <!-- <div class="py-2">
                </div> -->
                <div class="py-2">
                    
                </div>                
                <div class="disclamer" style="display:flex">
                  <span style="font-size: 1.5rem;margin-bottom: -2rem">*</span>
                  <span style="font-size: 0.9rem;color: rgba(0,0,0,.54);">Você sera redirecionado para o site do serviço de pagamento escolhido.</span>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-5 pt-4 pt-md-0">
              <h5>Detalhes do Plano</h5>
              <div class="card text-left" style="box-shadow:0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);">
                <div id="namePlan" class="card-header" style="background-color: transparent;">
                  Plano Basic Grand Chase Way
                </div>
                <div class="card-body">
                  <ul id="attributePlan">
                    
                  </ul>
                </div>
                <div class="card-footer">
                  <h5 id="precoPlan" class="card-title" style="font-family: 'Open Sans', sans-serif;">R$ 39,90</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php 
      require_once  'footer.php';
  ?>  
  <script src="pvendas/Js/geral.js"></script>
  <script>
    $(function () {
      choosePlan($('.bg-curso.premium').attr("alt"));
    })
    

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    $('.carousel').carousel({
      interval: false
    });

    function showDropdow(element) {
      // var src = $('img[alt="example"]').attr('src');
      var src = $(element).attr('src');
      var value = $(element).attr('value');
      var name = $(element).attr("data-original-title");

      // console.log(src);
      $(".toggle-button .text-button").hide();
      $(".img-basic-select").attr("src", src);
      $(".my-dropdown").toggle();
      $(".list-cheats-basic .bg-curso").removeClass("init");

      $(".bg-curso").each(function () {
        $(this).removeClass("active");
      });
      $(element).addClass("active");
      $(".bg-curso.basic").addClass("active");
      $(".ajust-center").attr('class', 'ajust-center');
      $(".ajust-center").addClass("basic");

      $(".price-basic").text("R$" + value);
      $(".name-basic").text(name);
    }

    $(".my-item > img").click(function () {
      showDropdow(this);
      // $(this).attr("alt");
      // console.log();
      choosePlan($(this).attr("alt"));
    });

    $(".toggle-button").click(function () {
      $(".my-dropdown").toggle();
      $('[data-toggle="popover"]').popover('hide');
    });

    $(".bg-curso.master, .bg-curso.premium").click(function () {
      $(".bg-curso").each(function () {
        $(this).removeClass("active");
      });
      $(this).addClass("active");

      var hasClass = $(this).hasClass("premium");
      $(".ajust-center").attr('class', 'ajust-center');
      if (hasClass) {
        $(".ajust-center").addClass("premium");
      } else {
        $(".ajust-center").addClass("master");
      }      
      choosePlan($(this).attr("alt"));
    });

    var resolution = $(window).width();
    if (resolution <= 425) {
      $('[data-toggle="tooltip"]').tooltip('disable');
    }

    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
      $('[data-toggle="popover"]').popover('hide');
    })

    $('#steps a[href="#passo2"]').on('shown.bs.tab', function (e) {
      
      if (resolution <= 425) {
        $('#popoverMobile').popover('show');
        $('#popoverMobile').popover('disable');
      } else {
        $('#popoverDescktop').popover('show');
        $('#popoverDescktop').popover('disable');
      }

      var fixmeTop = $('.fixme').offset().top; // get initial position of the element

      $(window).scroll(function () { // assign scroll event listener

        var currentScroll = $(window).scrollTop(); // get current position

        if (currentScroll >= fixmeTop) { // apply position: fixed if you
          $('.fixme').css({ // scroll to that element or below it
            position: 'fixed',
            top: '0',
            zIndex: '4',
            width: '100%',
            background: 'white'
          });
          $('.fixme > div').css({ // scroll to that element or below it
            padding: '0.4rem'
          });
        } else { // apply position: static
          $('.fixme').css({ // if you scroll above it
            position: 'static',
            width: 'inherit'
          });
          $('.fixme > div').css({ // scroll to that element or below it
            paddingRight: '15px',
            paddingLeft: '15px'
          });
        }

      });
        <?php if (isset($_GET['cheatId'])) { ?>
          var cheatId = <?php echo $_GET['cheatId']; ?>;    
          disparaImg(cheatId);
        <?php }else{ ?>
          var cheatId = '';    
        <?php } ?>
        
        function disparaImg(id) {
          var cheatImg = $('[alt="'+ id +'"]');
          // console.log(cheatImg);
          cheatImg.trigger( "click" ); 
          // $(".my-dropdown").toggle();
        }
    });

    $('#button-1').on('click', function (e) {
      $('#steps a[href="#passo2"]').tab('show');
      window.scrollTo(0, 0);
    })
    $('#button-2').on('click', function (e) {
      $('#steps a[href="#passo3"]').tab('show');
      selectedPlan(chosenPlan);
      window.scrollTo(0, 0);
    })

    $(document).on("click", function (event) {
      var $trigger = $(".dropdown");
      if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".my-dropdown").hide();
      }
    });
  </script>
</body>

</html>