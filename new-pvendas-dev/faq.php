<!doctype html>
<html lang="en">
<?php 
    require_once  'header.php';
?>
<link rel="stylesheet" href="pvendas/css/geral.css">
<link rel="stylesheet" href="pvendas/css/mobile.css">

<body id="faq">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9X2283"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <section class="top-logo">
        <div class="container">
            <div class="logo text-center pt-1">
                <img src="pvendas/imgs/cheats-logo-mini.png" alt="">
                <button type="button" class="btn btn-danger float-right m-3" onclick="location.href='?pg=checkout';">Assine Agora</button>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="col-lg-10 mx-auto">
            <h1 class="py-5 text-center">FAQ</h1>
            <div id="accordion py-3">
                <h4 class="my-3">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                        aria-controls="collapseOne">
                        E se eu não gostar da plataforma Cheats Place?
                    </button>
                </h4>                
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        Você terá garantia de 7 dias para analisar tudo o que seu plano contratado oferece e decidir se
                        deseja continuar na CheatsPlace.com ou não. Caso não queira continuar na nossa comunidade, basta
                        enviar um email para contato@cheatsplace.com e solicitar o reembolso do seu valor pago.
                    </div>
                </div>
                <h4 class="my-3">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="true"
                        aria-controls="collapse2">
                        Quais são as formas de pagamento?
                    </button>
                </h4>                
                <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                    <p>A compra de nossos produtos são realizadas através da plataforma de pagamentos Monetizze:<br>Aceitamos
                        Cartão de Crédito e boleto bancário*
                        <br><br>*Valores promociais são aceitos somente via Cartão de crédito.</p>
                    </div>
                </div>
                <h4 class="my-3">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="true"
                        aria-controls="collapse3">
                        Em quanto tempo após o pagamento eu terei acesso à plataforma Cheats Place?
                    </button>
                </h4>                
                <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <p>O acesso se dá através do e-mail informado no momento da compra e ocorre imediatamente após a
                        confirmação do pagamento por parte da plataforma da Monetizze.</p>
                        <p>Você receberá um e-mail de contato@cheatsplace.com com seus dados de acesso.</p>
                        <p>Pagamentos via boleto bancário podem demorar até 72 horas para confirmação.</p>
                    </div>
                </div>
                <h4 class="my-3">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse4" aria-expanded="true"
                        aria-controls="collapse4">
                        Em quais sistemas operacionais funciona?
                    </button>
                </h4>                
                <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <p>Os cheats disponibilizados na plataforma funcionam exclusivamente no sistema operacional <strong>Windows
                            7, 8, 8.1 e 10</strong>.</p>
                    </div>
                </div>
                <h4 class="my-3">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse5" aria-expanded="true"
                        aria-controls="collapse5">
                        E se eu quiser migrar de plano depois, eu posso?
                    </button>
                </h4>                
                <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <p>Sim, basta enviar um e-mail para contato@cheatsplace.com solicitando a migração do plano.</p>
                    </div>
                </div>
            </div>
            <div class="cta-forum text-center py-5">
                <h3>Não encontrou sua dúvida?</h3>
                <h5>Faça uma pergunta de pré-venda no nosso forum</h5>
                <button type="button" onclick="location.href='https://www.cheatsplace.com/forum/forum/29-perguntas-de-pr%C3%A9-venda/';" class="btn btn-info btn-lg mt-4">Fazer pergunta no forum</button>
            </div>
        </div>


    </section>
    <?php 
      require_once  'footer.php';
  ?>
    <script>
        function redirect(cheatId) {
            window.location.href = "?pg=cheatDetail&cheatId=" + cheatId;
        }

        function liveSearch() {
            // Declare variables
            var input, filter, ul, li, a, i;
            input = document.getElementById('myInput');
            filter = input.value.toUpperCase();
            ul = document.getElementById("myUL");
            li = ul.getElementsByTagName('li');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                h5 = li[i].getElementsByTagName("h5")[0];
                if (h5.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    </script>
</body>

</html>