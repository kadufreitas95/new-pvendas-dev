var all = "Acesso à todos os cheats de todos os jogos";
var onlyOne = "Acesso ao cheat para ";
var forumVIP = "Acesso ao Fórum VIP";
var suportBasic = "Suporte Básico";
var suportDedicado = "Suporte Dedicado";
var memberVip = "Membro em destaque no Fórum VIP";
var accessBeta = "Acesso ao conteúdo Beta";

var chosenPlan = null;

var plans = {
    premium:{
        id:1,
        name:"Premium",
        preco:"R$117,00",
        link_monetizze:"https://app.monetizze.com.br/checkout/DZF39773"
    },
    master:{
        id:2,
        name:"Master",
        preco:"R$67,00",
        link_monetizze:"https://app.monetizze.com.br/checkout/DGJ39772"
    },
    GCHL:{
        id:3,
        name:"Grand Chase History Latino",
        preco:"R$39,90",
        link_monetizze:"https://app.monetizze.com.br/checkout/DBW42157"
    },
    GCM:{
        id:4,
        name:"Grand Chase History Madness",
        preco:"R$39,90",
        link_monetizze:"http://mon.net.br/1yggz"
    },
    GCH:{
        id:5,
        name:"Grand Chase History",
        preco:"R$39,90",
        link_monetizze:"https://app.monetizze.com.br/checkout/DFX42154"
    },
    CF:{
        id:6,
        name:"CrossFire",
        preco:"R$29,90",
        link_monetizze:"https://app.monetizze.com.br/checkout/DDF45268"
    },
    PB:{
        id:7,
        name:"Point Blank",
        preco:"R$19,90",
        link_monetizze:"https://app.monetizze.com.br/checkout/DWV42109"
    },
    PBUSA:{
        id:8,
        name:"Point Blank United States",
        preco:"R$19,90",
        link_monetizze:"https://app.monetizze.com.br/checkout/DWE42153"
    }
};

function selectedPlan(idPLan) {
    var listAttr = [];
    switch (idPLan) {
        case 100://PREMIUM
            listAttr.push('Acesso a 3 computadores');
            listAttr.push(all);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            listAttr.push(suportDedicado);
            listAttr.push(memberVip);
            listAttr.push(accessBeta);
            addAttr(listAttr, plans.premium);
            break;
        case 99://MASTER
            listAttr.push('Acesso a 2 computadores');
            listAttr.push(all);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.master);
            break;
        case 9://GCHL
            listAttr.push('Acesso a 1 computador');
            listAttr.push(onlyOne + plans.GCHL.name);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.GCHL);
            break;
        case 10://GCM
            listAttr.push('Acesso a 1 computador');
            listAttr.push(onlyOne + plans.GCM.name);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.GCM);
            break;
        case 8://GCH
            listAttr.push('Acesso a 1 computador');
            listAttr.push(onlyOne + plans.GCH.name);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.GCH);
            break;
        case 13://CF
            listAttr.push('Acesso a 1 computador');
            listAttr.push(onlyOne + plans.CF.name);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.CF);
            break;
        case 11://PB
            listAttr.push('Acesso a 1 computador');
            listAttr.push(onlyOne + plans.PB.name);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.PB);
            break;
        case 12://PBUSA
            listAttr.push('Acesso a 1 computador');
            listAttr.push(onlyOne + plans.PBUSA.name);
            listAttr.push(forumVIP);
            listAttr.push(suportBasic);
            addAttr(listAttr, plans.PBUSA);
            break;
        default:
            break;
    }
}


function addAttr(listAttr, plan) {
    var namePLan = document.getElementById('namePlan');
    var attributePlan = document.getElementById('attributePlan');
    var precoPlan = document.getElementById('precoPlan');
    var btnMonetizze = document.getElementById('btnMonetizze');

    namePLan.innerHTML = plan.name;
    precoPlan.innerHTML = plan.preco;
    btnMonetizze.setAttribute('onclick', "location.href='"+ plan.link_monetizze +"';");

    attributePlan.innerHTML = '';

    listAttr.forEach(attr => {

        var liAttr = document.createElement('li');
        var liText = document.createTextNode(attr);

        liAttr.appendChild(liText);

        attributePlan.appendChild(liAttr);     
    });
}

function choosePlan(nickPLan) {    
    chosenPlan = parseInt(nickPLan); 
    console.log(chosenPlan);    
}

// window.onload = function() {
//     selectedPlan(3);
// };


