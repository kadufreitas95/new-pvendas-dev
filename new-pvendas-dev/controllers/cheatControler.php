<?php

class CheatController {

    private $url;
    private $apiCall;
    
    public function __construct() {
        require_once 'apiCall.php';
        $this->apiCall = new ApiCall();
        $this->url = '/cheats/freeRead';
    }

    public function listAllCheats(){
        return $this->apiCall->apiRequest($this->url);
    }

    public function cheatsDetail($id){
        $cheat_id = (int) $id;
        return $this->apiCall->apiRequest($this->url .'/'. $cheat_id);
    }
   
}