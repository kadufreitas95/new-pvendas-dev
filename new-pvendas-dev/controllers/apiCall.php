<?php

class ApiCall {    

    public $communityUrl = "https://api.cheatsplace.com/dev";
    // public $communityUrl = "https://api.cheatsplace.com/last";

    public function apiRequest($url, $params = NULL ){
        $curl = curl_init();

        $full_url = $this->communityUrl . $url;

        if ($params != NULL) {
            $full_url .= $params;   
        }

        curl_setopt_array( $curl, array(
            CURLOPT_URL => $full_url,
            CURLOPT_RETURNTRANSFER  => TRUE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
            )
        ) );

        $response = curl_exec( $curl );
        curl_close($curl);
        return json_decode($response, true);
    }
}