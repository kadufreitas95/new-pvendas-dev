<!doctype html>
<html lang="en">
<?php 
    require_once  'header.php';
?>
<link rel="stylesheet" href="pvendas/css/cheatsDetail.css">
<link rel="stylesheet" href="pvendas/css/mobileChatsDetail.css">

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9X2283"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <section class="hero col-lg-12">
    <section class="top-logo">
      <div class="container">
        <div class="logo text-center pt-1">
        <a href="?pg=home"><img src="pvendas/imgs/cheats-logo-mini.png" alt=""></a>
        </div>
      </div>
    </section>
    <div class="row" style="overflow:hidden">
      <div class="primay-colum col-lg-6 offset-xl-1">
        <h1 class="text-white title" style="font-size: 2rem;">Hack VIP para <?php echo $response['name'] ?></h1>
        <div class="video py-4">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="<?php echo $response[0] ?>" allowfullscreen></iframe>
          </div>
        </div>
        <div class="features py-5">
          <h2 class="text-white title">FEATURES</h2>
          <div class="card mt-4">
            <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
            <div class="card-body">
              <div class="row">
                <div class="col-lg-4">
                  <?php for ($i=0; $i < count($response['features']); $i++) { ?>

                  <?php 
                      if (($i % 7) == 0 && $i != 0) { 
                        echo '</div>';
                        echo '<div class="col-lg-4">'; 
                      } ?>
                  <p>
                    <?php echo $response['features'][$i]['name'] ?>
                  </p>

                  <?php } echo '</div>';?>
                </div>
              </div>
            </div>
          </div>
          <div class="sobre py-4" style="display:none">
            <h2 class="text-white title">SOBRE</h2>
            <p class="text-white mt-4">Praesent ultrices bibendum rutrum. Donec id mauris arcu. Aenean sollicitudin
              nulla
              non
              tortor laoreet, in vehicula elit faucibus. Maecenas cursus ante imperdiet iaculis ornare. Quisque blandit
              varius odio, ut
              semper velit suscipit ac. Cras at tortor arcu. Vestibulum ante ipsum primis in faucibus orci luctus et
              ultrices posuere cubilia Curae; In a dolor magna. Phasellus at lorem vitae justo aliquet lobortis.</p>
          </div>
          <div class="depoimentos py-4 text-white">
            <h2 class="title">DEPOIMENTOS</h2>
            <div id="carouselExampleIndicators" class="carousel slide mt-4" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="col-lg-8 col-10 offset-1 offset-md-2 col-md-8 py-5">
                    <p>O que tenho a dizer sobre o Loader e o Cheat vip da CheatsPlace é que é uma coisa muito prática
                      e fácil de se utilizar, não dá disconnect, não é detected in-game e o conteúdo oferecido em si é
                      bastante seguro, estou usando tranquilamente no Grand Chase Way e não estou tendo problemas
                      nenhum em relação ao mesmo, e em breve, se Deus quiser, estarei no Combat Arms também na Cheats
                      Place! Valeu a toda a staff da Cheats Place, principalmente o @crackiller por estar administrando
                      bem essa nova plataforma, e espero em breve estar junto com amigos para usufruirmos juntos!
                      Abraços e parabéns!</p>
                    <div class="text-center">
                      <div class="img-user">
                        <img src="pvendas/imgs/Ryan100.png" alt="">
                      </div>
                      <span>Andeer Silva</span>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="col-lg-8 col-10 offset-1 offset-md-2 col-md-8 py-5">
                    <p>Adquiri o Cheat para Grand Chase Way e tenho me surpreendido. Depois da compra, em menos de 1
                      hora, ja estava tudo funcionando de boas, o suporte é bem rápido, de primeira. O hack vip está valendo cada
                      centavo! Parabéns a equipe Cheats Place.</p>
                    <div class="text-center">
                      <div class="img-user">
                        <img src="pvendas/imgs/Ronan100.png" alt="">
                      </div>
                      <span>Teknoia2121</span>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="col-lg-8 col-10 offset-1 offset-md-2 col-md-8 py-5">
                    <p>Meu irmão que me mostrou o cheat. Assinei porque no jogo que jogo estava tendo um evento e
                      ninguém do jogo conseguia passar, quando assinei o cheat passei do boa. Estou muito satisfeito
                      com a Cheats Place.</p>
                    <div class="text-center">
                      <div class="img-user">
                        <img src="pvendas/imgs/PointBlank21.ico" alt="">
                      </div>
                      <span>Rodrigoyou</span>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="col-lg-8 col-10 offset-1 offset-md-2 col-md-8 py-5">
                    <p>Sempre acompanhei a antiga playcheat e usei os hacks pra pbbr, conheço a muito tempo. Quando
                      lançaram a Cheats Place não tive dúvidas alguma, resolvi assinar logo o hack vip para Point Blank.</p>
                    <div class="text-center">
                      <div class="img-user">
                        <img src="pvendas/imgs/PointBlank25.ico" alt="">
                      </div>
                      <span>Kaairos</span>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="col-lg-8 col-10 offset-1 offset-md-2 col-md-8 py-5">
                    <p>Pra jogar a maioria dos jogos é necessário ter tempo pra treinar, seja em fps ou mmorpg e eu não
                      tenho muito tempo pra isso. Além disso, é muito divertido usar hack haha. Tive bastante
                      recomendações da Cheats Place e hoje sei que é a melhor opção para mim.</p>
                    <div class="text-center">
                      <div class="img-user">
                        <img src="pvendas/imgs/PointBlank1.ico" alt="">
                      </div>
                      <span>Guilherme</span>
                    </div>
                  </div>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <hr>
          <div class="outros-cheats py-4 text-white">
            <h6>OUTROS CHEATS / HACKS</h6>
            <div id="carouselOutrosCheats" class="carousel slide mt-1" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselOutrosCheats" data-slide-to="0" class="active"></li>
                <li data-target="#carouselOutrosCheats" data-slide-to="1"></li>
                <!-- <li data-target="#carouselOutrosCheats" data-slide-to="2"></li> -->
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="col-10 offset-1 col-lg-8 col-md-8 offset-md-2 py-5">
                    <div class="row justify-content-around">
                      <div class="img-game col-4 col-md-2 col-lg-3 col-xl-2">
                        <a href="?pg=cheatDetail&cheatId=13"><img src="pvendas/imgs/games-insignias/CF.png" alt=""></a>
                      </div>
                      <div class="img-game col-4 col-md-2 col-lg-3 col-xl-2">
                        <a href="?pg=cheatDetail&cheatId=6"><img src="pvendas/imgs/games-insignias/GCM.png" alt=""></a>
                      </div>
                      <div class="img-game col-4 col-md-2 col-lg-3 col-xl-2">
                        <a href="?pg=cheatDetail&cheatId=8"><img src="pvendas/imgs/games-insignias/GCH.png" alt=""></a>
                      </div>
                      <div class="img-game col-md-2 col-lg-3 col-xl-2">
                        <a href="?pg=cheatDetail&cheatId=9"><img src="pvendas/imgs/games-insignias/GCHL.png" alt=""></a>
                      </div>
                      <div class="img-game col-md-2 col-xl-2">
                        <a href="?pg=cheatDetail&cheatId=11"><img src="pvendas/imgs/games-insignias/PB.png" alt=""></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="col-10 offset-1 col-lg-8 col-md-8 offset-md-2 py-5">
                    <div class="row justify-content-around">
                      <div class="img-game col-4 col-md-2 col-lg-3 col-xl-2">
                        <a href="?pg=cheatDetail&cheatId=12"><img src="pvendas/imgs/games-insignias/PBUSA.png" alt=""></a>
                      </div>
                      <!-- <div class="img-game col-4 col-md-2 col-lg-3 col-xl-2">
                      <a href=""><img src="imgs/icon-Grand-Chase.png" alt=""></a>
                    </div>
                    <div class="img-game col-4 col-md-2 col-lg-3 col-xl-2">
                      <a href=""><img src="imgs/icon-Grand-Chase.png" alt=""></a>
                    </div>
                    <div class="img-game col-md-2 col-lg-3 col-xl-2">
                      <a href=""><img src="imgs/icon-Grand-Chase.png" alt=""></a>
                    </div>
                    <div class="img-game col-md-2 col-xl-2">
                      <a href=""><img src="imgs/icon-Grand-Chase.png" alt=""></a>
                    </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselOutrosCheats" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselOutrosCheats" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <footer class="footer features-section py-5">
            <div class="text-center col-md-12" style="color:#8f959e">
              <div class="contanier">
                <div class="col-lg-12">
                  <div class="col-lg-12  mx-lg-auto">
                    <div class="contato  text-left">
                      <p>Duvidas?<a href="mailto:contato@cheatsplace.com"> contato@cheatsplace.com</a></p>
                    </div>
                    <div class="row justify-content-center">
                      <div class="col-lg-4 col-md-4">
                        <button type="button" class="btn btn-link col-12 text-left" onclick="location.href='?pg=faq';">Perguntas
                          Frequentes</button>
                        <button type="button" class="btn btn-link col-12 text-left" onclick="location.href='https://www.cheatsplace.com/termos-de-uso.php';">Termos
                          de Uso</button>
                        <button type="button" class="btn btn-link col-12 text-left" onclick="location.href='https://www.cheatsplace.com/politica-de-privacidade.php';">Política
                          de Privacidade </button>
                      </div>
                      <div class="col-lg-4 col-md-4">
                        <button type="button" class="btn btn-link col-12 text-left disabled-link">Trabalhe Conosco</button>
                        <button type="button" class="btn btn-link col-12 text-left disabled-link">Seja um Parceiro</button>
                      </div>
                      <div class="col-lg-4 col-md-4">
                        <button type="button" class="btn btn-link col-12 text-left disabled-link">Blog</button>
                        <button type="button" class="btn btn-link col-12 text-left forum">Fórum</button>
                      </div>
                    </div>
                    <div class="pt-5 text-center">
                      <span class="left">Cheats Place © 2018. Todos os direitos reservados.</span>
                    </div>
                  </div>
                </div>
              </div>
          </footer>
        </div>
        <div class="col-lg-5 col-xl-4" id="coluna-lateral-fixed">
        <?php 
          switch ($response['status']) {
            case 1:
                $status = "online";
                break;
            case 2:
                $status = "maintenance";
                break;
            case 3:
                $status = "offline";
                break;
          } ?>
          <img class="bg" src="pvendas/imgs/<?php echo $response[1] ?>" alt="">
          <!-- <img class="bg" src="imgs/wallpaperCF.png" alt=""> -->
          <div class="info-cheat">
            <div class="card col-lg-12">
              <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
              <div class="card-body">
                <h5 class="card-title text-center">
                  <?php echo $response['name'] ?>
                </h5>
                <div class="row justify-content-between">
                  <div class="col-5 text-left">
                    <span>
                      <div class="ball-status <?php echo $status ?>"></div> <?php echo $status ?>
                    </span>
                  </div>
                  <div class="col-7 text-right">
                    <span>atualizado: Há <?php echo $intervalo->d ?> dias</span>
                  </div>
                </div>
                <p class="card-text"></p>
              </div>
            </div>
            <a href="?pg=checkout&cheatId=<?php echo $_GET['cheatId'] ?>" class="btn btn-lg btn-block mt-3">assine
              agora</a>
          </div>
        </div>
      </div>
  </section>
  <div class="info-cheat-mobile">
    <div class="card col-lg-12">
      <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
      <div class="card-body p-0 p-sm-0">
        <h5 class="card-title text-center"><?php echo $response['name'] ?></h5>
        <div class="row justify-content-between">
          <div class="col-5 text-left">
            <span>
              <div class="ball-status <?php echo $status ?>"></div> <?php echo $status ?>
            </span>
          </div>
          <div class="col-7 text-right">
            <span>atualizado: Há <?php echo $intervalo->d ?> dias</span>
          </div>
        </div>
        <p class="card-text"></p>
      </div>
    </div>
    <a href="?pg=checkout" class="btn btn-lg btn-block">assine agora</a>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="pvendas/jquery/jquery.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
  <script src="pvendas/bootstrap4/js/popper.min.js"></script>
  <script src="pvendas/bootstrap4/js/bootstrap.min.js"></script>
  <script>
    $('.carousel').carousel({
      interval: false
    });
  </script>
</body>

</html>