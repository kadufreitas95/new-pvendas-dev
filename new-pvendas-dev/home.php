<!doctype html>
<html lang="en">
<?php 
    require_once  'header.php';
?>
<link rel="stylesheet" href="pvendas/css/geral.css">
<link rel="stylesheet" href="pvendas/css/mobile.css">

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9X2283"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <section class="top-logo">
    <div class="container">
      <div class="logo text-center pt-1">
        <img src="pvendas/imgs/cheats-logo-mini.png" alt="">
      </div>
    </div>
  </section>
  <section class="bg">
    <div class="img">
      <img src="pvendas/imgs/cdmok.jpg" alt="bg">
      <div class="gradient"></div>
    </div>
    <div class="cta">
      <div class="cta-text">
        <h1 class="cta-header">Seu próximo nível, agora.</h1>
        <p class="cta-sub-header">Sua Plataforma número #1 de Cheats.</p>
      </div>
      <div class="cta-link">
        <button id="button-1" type="button" onclick="location.href='?pg=checkout';" class="btn css-button mx-auto">Assine
          agora&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-right"></i></button>
      </div>
    </div>
  </section>
  <section class="features py-5">
    <div class="container">
      <div class="header text-center">
        <h3 style="text-transform:uppercase;">Por que a Cheats Place é a sua melhor escolha</h3>
      </div>
      <div class="row pt-5">
        <div class="col-md-6 col-lg-4 pb-lg-4 pb-sm-5">
          <div class="media py-3 py-sm-0">
            <!-- <img class="align-self-start mr-3" src="..." alt="Generic placeholder image"> -->
            <i class="fas fa-lock fa-4x align-self-start mr-3"></i>
            <div class="media-body">
              <h5 class="mt-0">Segurança</h5>
              <p>Todos nossos cheats estão protegidos por um algotimo único. Utilizamos de várias camadas de proteção
                para garantir sua seguraça durante o jogo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 pb-lg-4 pb-sm-5">
          <div class="media py-3 py-sm-0">
            <i class="fas fa-exclamation fa-4x align-self-start mr-3"></i>
            <div class="media-body">
              <h5 class="mt-0">Ativação instantânea</h5>
              <p>A ativação da sua conta ocorre automaticamente e imediatamente após a confirmação da sua assinatura,
                sem depender de nenhum passo manual ou humano. Assine e jogue agora mesmo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 pb-sm-5">
          <div class="media py-3 py-sm-0">
            <!-- <img class="align-self-start mr-3" src="..." alt="Generic placeholder image"> -->
            <i class="fas fa-headset fa-4x align-self-start mr-3"></i>
            <div class="media-body">
              <h5 class="mt-0">Suporte 24/7</h5>
              <p>Temos uma equipe qualificada para apoio disponível 24 horas por dia e 7 dias por semana para lhe
                ajudar sempre que preciso.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 pb-lg-4 pb-sm-5">
          <div class="media py-3 py-sm-0">
            <i class="fas fa-door-open fa-4x align-self-start mr-3 resize"></i>
            <div class="media-body">
              <h5 class="mt-0">Sem compromisso</h5>
              <p>Se você decidir não continuar a assinatura, tudo bem. Sem compromisso. Cancele online quando quiser.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="media py-3 py-sm-0">
            <i class="fas fa-medal fa-4x align-self-start mr-3"></i>
            <div class="media-body">
              <h5 class="mt-0">Plataforma Dedicada</h5>
              <p>Contamos com uma plataforma totalmente dedica à nossos assinantes: Painel exclusivo, área VIP no Fórum
                e Discord e diversos prêmios para diferentes tipos de programas de bonificação exclusivos da Cheats
                Place.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="media py-3 py-sm-0">
            <i class="fas fa-user-check fa-4x align-self-start mr-3 resize"></i>
            <div class="media-body">
              <h5 class="mt-0">Usabilidade</h5>
              <p>Nossos cheats contêm Interfaces simples para você ativar as funcionalidades do cheat que você quiser
                rapidamente durante a partida.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="row pt-0 pt-sm-5">
        
      </div> -->
      <div class="cta-second text-center py-3">
        <button id="button-1" type="button" onclick="location.href='?pg=checkout';" class="btn css-button second">Assine
          Agora</button>
      </div>
    </div>
  </section>
  <section class="search py-5">
    <div class="star"></div>
    <div class="container">
      <div class="header text-center">
        <h3>ENCONTRE SEU JOGO</h3>
      </div>
      <div class="form pt-3">
        <form class="form-inline my-2 my-lg-0" style="display:none">
          <input id="myInput" class="form-control col-md-8 mx-auto" type="search" placeholder="Encontre seu jogo"
            aria-label="Search" onkeyup="liveSearch()">
          <!-- <button class="btn btn-outline-secondary my-2 my-sm-0 mr-auto" type="submit">Buscar</button> -->
        </form>
        <div class="col-md-10 col-lg-8 mx-auto pt-4">
          <ul id="myUL" class="list-unstyled games-list">
            <div class="row justify-content-center">
              <?php for ($i=0; $i < count($response); $i++) { 
                if ($response[$i]['status'] == 1) {?>
                  <li class="media col-md-10">
                <!-- <img class="align-self-center" src="./imgs/logo-icon.png" alt="Generic placeholder image"> -->
                <div class="media-body my-auto" onclick="redirect(<?php echo $response[$i]['id']?>)">
                  <!-- <img src="./imgs/icon-Point-Blank.png" alt=""> -->
                  <h5 class="mt-0 mb-1">
                    <?php echo $response[$i]['name'] ?>
                  </h5>
                </div>
                <div class="border-inner"></div>
              </li>
              <?php }                   
              }?>
            </div>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <?php 
      require_once  'footer.php';
  ?>
  <script>
    function redirect(cheatId) {
      window.location.href = "?pg=cheatDetail&cheatId=" + cheatId;
    }

    function liveSearch() {
      // Declare variables
      var input, filter, ul, li, a, i;
      input = document.getElementById('myInput');
      filter = input.value.toUpperCase();
      ul = document.getElementById("myUL");
      li = ul.getElementsByTagName('li');

      // Loop through all list items, and hide those who don't match the search query
      for (i = 0; i < li.length; i++) {
        h5 = li[i].getElementsByTagName("h5")[0];
        if (h5.innerHTML.toUpperCase().indexOf(filter) > -1) {
          li[i].style.display = "";
        } else {
          li[i].style.display = "none";
        }
      }
    }
  </script>
</body>

</html>